FROM node:18-alpine AS builder

LABEL maintainer="Syahril <syahrilsymd@gmail.com>"

WORKDIR /usr/src
COPY package*.json ./
RUN yarn install --frozen-lockfile
COPY . .
COPY src src
RUN yarn build

FROM node:18-alpine
RUN apk add --no-cache tini
WORKDIR /usr/src
RUN chown node:node .
USER node
COPY package*.json ./
RUN yarn install
COPY --from=builder /usr/src dist/

ENTRYPOINT [ "/sbin/tini","--", "node", "dist/index.js" ]
